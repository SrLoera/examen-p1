from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import Jugadores
# Create your views here.

def index(request):
    return render(request, "home/index.html",{})

def listJugador(request):
    context={
    "player": Jugadores.objects.all()
    }
    return render(request,"home/ListJugadores.html",context)
