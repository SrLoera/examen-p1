from django.contrib import admin

# Register your models here.
from .models import Jugadores
from .models import Equipos
from .models import Estadios


admin.site.register(Jugadores)
admin.site.register(Equipos)
admin.site.register(Estadios)
