from django.db import models
from django.conf import settings
# Create your models here.
class Jugadores(models.Model):
    nombre=models.CharField(max_length=256)
    posicion=models.CharField(max_length=250)
    numero=models.CharField(max_length=256)
    peso=models.CharField(max_length=250)
    altura=models.CharField(max_length=256)
    equipo=models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

class Estadios(models.Model):
    estadio = models.CharField(max_length=250)
    direccion=models.CharField(max_length=256)
    capacidad=models.CharField(max_length=250)
    estado=models.CharField(max_length=256)
    ciudad=models.CharField(max_length=250)
    def __str__(self2):
        return self2.estadio


class Equipos(models.Model):
    nombre_equipo=models.CharField(max_length=200)
    mascota=models.CharField(max_length=200)
    sede=models.CharField(max_length=200)
    propietario=models.CharField(max_length=200)
    entrenador=models.CharField(max_length=200)
    camponatos_nacionales=models.CharField(max_length=200)
    super_bowls=models.CharField(max_length=200)

    def __str__(self3):
        return self3.nombre_equipo
